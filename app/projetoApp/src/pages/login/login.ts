import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {RegisterPage} from "../register/register";
import {HomePage} from "../home/home";

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {
  username:string;
  password:string;
  constructor(public navCtrl: NavController) {

  }
  login(){
    console.log("E-mail" + this.username);
    console.log("Senha" + this.password);
    this.navCtrl.setRoot(HomePage);
  }

  goRegister(){
    this.navCtrl.push(RegisterPage);
  }

}
