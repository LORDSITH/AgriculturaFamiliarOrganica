import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {ProductCardPage} from "../product-card/product-card";
import { ModalController } from 'ionic-angular';

/**
 * Generated class for the ProductPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-product',
  templateUrl: 'product.html',
})
export class ProductPage {

  constructor(public navCtrl: NavController, public modalCtrl: ModalController) { }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ProductsPage');
  }

  new_product() {
    let modal = this.modalCtrl.create(ProductCardPage);
    modal.present();

    modal.onDidDismiss(data => {
      alert('Closed with data:' + JSON.stringify(data));
    });
  }

}
